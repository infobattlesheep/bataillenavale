package Battlesheep;

/**
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 *
 * Classe du cuirras�
 */
public class Cuirrass� extends Bateau
{
  public Cuirrass�(int x, int y, int longueur, boolean horizontal)
  {
    super(x, y, longueur, horizontal);
  }

  public String getType()
  {
    return "cuirass�";
  }
}