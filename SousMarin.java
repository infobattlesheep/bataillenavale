package Battlesheep;

/**
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 *
 * Classe du sous-marin
 */
public class SousMarin extends Bateau
{
  public SousMarin(int x, int y, int longueur, boolean horizontal)
  {
    super(x, y, longueur, horizontal);
  }

  public String getType()
  {
    return "sous-marin";
  }

}