package Battlesheep;

/**
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 *
 * Classe du croiseur
 */
public class Croiseur extends Bateau
{
  public Croiseur(int x, int y, int longueur, boolean horizontal)
  {
    super(x, y, longueur, horizontal);
  }

  public String getType()
  {
    return "croiseur";
  }
}